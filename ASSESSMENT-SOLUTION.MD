# QA Engineer Assessment Solution

## Introduction

Use this file to document your solution and findings.

Name: Joost van Dieten

# Remarks

### General remarks
 - I like the idea of being assessed before being hired; This says that you want to hire the best people!
 - angular 7 version used; angular 10 latest and greatest maybe the project should be updated
 - missing unit tests spec files should be added for components and services I think best test coverage can
 be reached with a combination of unit integration and e2e tests.

### about Protractor:
 If you look at Protractor you can get the feeling that, in spite of all the effort that the Open Source Community is putting in it,
  it is not being maintained by Google anymore. If you look at the repository you see the following facts:
    - the amount of issues is increasing
    - the amount of PR's is increasing
    - the activity in the project is decreasing

 For this reason most projects are already migrating to other test frameworks. This
 [artircle](https://dev.to/davert/5-reasons-you-should-not-use-protractor-in-2019-3l4b) also explains why you should not use it anymore.
 Alternatives depending on needs would be webdriverio, playwright, cypress or testcafe.

# Setup

### 1 resolving feature files
 specs: [
    './e2e/features/*/*.feature'
  ]

  will not resolve all feature files in setup changed to

   specs: [
      './e2e/features/**/*.feature'
    ]

  This will resolve all features

...

### 2 install protractor-cucumber-framework and add to protractor.conf.js

install protractor-cucumber-framework

```npm i protractor-cucumber-framework --save-dev```

add to protractor.conf.js

```frameworkPath: require.resolve('protractor-cucumber-framework')```

### 3 create reporting folder if it doesn't exist

The problem is that the cucumber results are placed in a separate report folder,
if this folder doesn't exist your build will fail.

I placed the following in the onPrepare function of protractor.conf.js. This way the
directory will be created if it doesn't exist using default node package fs.

```
       if (!require('fs').existsSync(reportPath)) {
         require('fs').mkdirSync(reportPath)
       }
```

# Testing

## My test code general remarks

 - No use of browser.sleep this should be avoided to prevent test flakyness. Use explicit waits for element
 - Used only css selectors
 - Tried to explain all choices I have made in my code comments

## Findings


### 1 Scenario failure:

  Specification:
  *	When you search for either a character or a planet and you get one or more results for it, clear the “Search form” and hit the Search button again, you should then get an empty result list (previous search results are removed).

  The scenario below failed and should have passed according to specification:

    Scenario: Existing results change search type and search again
       Given I select "planets" for search
       When I search for "Tatooine"
       Then I should have 1 planet results
       Given I select "people" for search
       When I click search button
       Then I should see no result found

### 2 No error handling:

If you change url with unknown searchType i.e. http://localhost:4200/?searchType=unknown&query=mus The page is not handling the error correctly
page still says Loading.. but error already in console.

### 3 Searching
Observed that Searching is not case-sensitive not stated in the specs if it should be or not verify with PO.
