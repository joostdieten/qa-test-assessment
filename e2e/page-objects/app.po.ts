// generic app functionality loading error etc
import { $ } from 'protractor';

module.exports = {
  get loadingDivEl() {
    // added id to the div in app template to make it more robust
    return $('#loadingDiv');
  },
  get notFoundDivEl() {
    // added id to the div in app template to make it more robust
    return $('#notFoundDiv');
  },
};
