// use protractor element shorthand css selector
import { $ } from 'protractor';

module.exports = {
  get peopleRadioInput() {
    return $('#people');
  },
  get planetsRadioInput() {
    return $('#planets');
  },
  get searchField() {
    return $('#query');
  },
  get searchBtn() {
    return $('button');
  }
};
