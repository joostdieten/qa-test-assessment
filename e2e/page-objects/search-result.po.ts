// $$ is element.all equivalent
// created a generic search result page object so it can be reused by multiple search options types i.e. people and planets
import { $$, ElementFinder } from 'protractor';

module.exports = {
  get allCharacterResults() {
    return $$('app-character');
  },
  get allPlanetResults() {
    return $$('app-planet');
  },
  titleFromResultEl(resultEl: ElementFinder) {
    return resultEl.$('.card-subtitle').getText();
  },
  resultRowTextAtIndex(resultEl: ElementFinder, index: number) {
    return resultEl.$$('.row').get(index).getText();
  }
};
