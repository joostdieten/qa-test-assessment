// Helper functions for protractor waits for staleness of elements etc
import { browser, ElementFinder, protractor } from 'protractor';
// standard protractor timeout that can be used in steps pages
export const waitTimeout = 5000;
// function with explicit wait on given element
export async function waitForElementNotPresent(element: ElementFinder): Promise<void> {
  await browser.wait(protractor.ExpectedConditions.stalenessOf(element), waitTimeout);
}
// helper function to clear al data from an input field due to non functioning element.clear()
// see https://github.com/angular/protractor/issues/5234
export async function clearFieldWithBackspace(inputEl: ElementFinder, spaces?: number) {
  return inputEl.getAttribute('value').then((text) => {
    let spacesToGoBack = spaces || text.length;
    do {
      inputEl.sendKeys(protractor.Key.BACK_SPACE);
      spacesToGoBack--;
    } while (spacesToGoBack > 0);
  });
}
