// separate chai imports so we only need 1 import in other files
import * as chaiExpect from 'chai';
import * as cap from 'chai-as-promised';

chaiExpect.use(cap);

export const expect = chaiExpect.expect;
