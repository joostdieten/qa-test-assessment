import { Given } from 'cucumber';
import { browser } from 'protractor';
import { options } from '../helpers/cucumber-options';

// type and name variable
Given('I navigate to {string}', options, async (host: string) => {
  await browser.get('http://' + host + ':4200/');
  await browser.waitForAngular();
});
