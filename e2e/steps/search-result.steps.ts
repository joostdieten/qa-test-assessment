import { TableDefinition, Then } from 'cucumber';
import { options } from '../helpers/cucumber-options';
import { expect } from '../helpers/chai-imports';
import { waitForElementNotPresent } from '../helpers/protractor-functions';

const searchResultPO = require('../page-objects/search-result.po');
const appPO = require('../page-objects/app.po');

Then('I should see no result found', options, async() => {
  await expect(appPO.notFoundDivEl.isPresent()).to.eventually.be.true;
});

Then('I should have {int} planet results', options, async(items: number) => {
  await expect(searchResultPO.allPlanetResults.count()).to.eventually.equal(items);
});

Then('I should have {int} character results', options, async(items: number) => {
  await expect(searchResultPO.allCharacterResults.count()).to.eventually.equal(items);
});

// use datatable to verify characters in a clear way
Then('I should see character results', options, async (characters: TableDefinition) => {
  const characterHashes = characters.hashes();
  // be sure the loading is done
  await waitForElementNotPresent(appPO.loadingDivEl);
  let index = 0;
  for (const row of characterHashes) {
    const characterResult = await searchResultPO.allCharacterResults.get(index);
    await expect(searchResultPO.titleFromResultEl(characterResult)).to.eventually.contain(row.name);
    await expect(searchResultPO.resultRowTextAtIndex(characterResult, 0)).to.eventually.contain(row.gender);
    await expect(searchResultPO.resultRowTextAtIndex(characterResult, 1)).to.eventually.contain(row.birth_year);
    await expect(searchResultPO.resultRowTextAtIndex(characterResult, 2)).to.eventually.contain(row.eye_color);
    await expect(searchResultPO.resultRowTextAtIndex(characterResult, 3)).to.eventually.contain(row.skin_color);
    index++;
  }
});

// use datatable to verify multiple planets in a clear way
Then('I should see planet results', options, async (planets: TableDefinition) => {
  const planetHashes = planets.hashes();
  // be sure the loading is done
  await waitForElementNotPresent(appPO.loadingDivEl);
  let index = 0;
  for (const row of planetHashes) {
    const planetResult = await searchResultPO.allPlanetResults.get(index);
    await expect(searchResultPO.titleFromResultEl(planetResult)).to.eventually.contain(row.name);
    await expect(searchResultPO.resultRowTextAtIndex(planetResult, 0)).to.eventually.contain(row.population);
    await expect(searchResultPO.resultRowTextAtIndex(planetResult, 1)).to.eventually.contain(row.climate);
    await expect(searchResultPO.resultRowTextAtIndex(planetResult, 2)).to.eventually.contain(row.gravity);
    index++;
  }
});

