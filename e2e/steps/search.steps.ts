// used imports instead off require
import { Given, When } from 'cucumber';

import { options } from '../helpers/cucumber-options';
import { browser, protractor } from 'protractor';
import { clearFieldWithBackspace } from '../helpers/protractor-functions';

const searchFormPO = require('../page-objects/search-form.po');

Given('I select {string} for search', options, async (option: 'people' | 'planets') => {
  if (option === 'people') {
    await searchFormPO.peopleRadioInput.click();
  } else if (option === 'planets') {
    await searchFormPO.planetsRadioInput.click();
  }
});

When('I clear my search and search again', options, async () => {
  await clearFieldWithBackspace(searchFormPO.searchField);
  await searchFormPO.searchBtn.click();
});

When('I click search button', options, async () => {
  await searchFormPO.searchBtn.click();
});

// made generic search glue step
When('I search for {string}', options, async (searchString: string) => {
  await searchFormPO.searchField.sendKeys(searchString);
  await searchFormPO.searchBtn.click();
});


When('I search for {string} via keypress enter', options, async (searchString: string) => {
  await searchFormPO.searchField.sendKeys(searchString);
  await browser.actions().sendKeys(protractor.Key.ENTER).perform();
});
