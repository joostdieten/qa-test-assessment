Feature: Search for a Star Wars planet

  Background:
    Given I navigate to "localhost"
    And I select "planets" for search

  Scenario: Search for exact planet
    When I search for "Tatooine"
    Then I should see planet results
      | name     | population | climate | gravity    |
      | Tatooine | 200000     | arid    | 1 standard |

  Scenario: Search for partial planet
    When I search for "Ta"
    Then I should have 5 planet results

  Scenario: Search for a non existing planet
    When I search for "Mars"
    Then I should see no result found


