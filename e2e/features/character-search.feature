Feature: Search for a Star Wars character

  Background:
    Given I navigate to "localhost"
    And I select "people" for search

  Scenario: Search for exact character
    When I search for "Luke Skywalker"
    Then I should see character results
      | name           | gender | birth_year | eye_color | skin_color |
      | Luke Skywalker | male   | 19BBY      | blue      | fair       |

  Scenario: Search for partial character
    When I search for "Skywalker"
    Then I should see character results
      | name             | gender | birth_year | eye_color | skin_color |
      | Luke Skywalker   | male   | 19BBY      | blue      | fair       |
      | Anakin Skywalker | male   | 41.9BBY    | blue      | fair       |
      | Shmi Skywalker   | female | 72BBY      | brown     | fair       |

  Scenario: Search for a non existing character
    When I search for "Tester"
    Then I should see no result found


