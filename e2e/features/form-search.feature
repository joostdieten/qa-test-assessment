Feature: Generic form search functionality

  Background:
    Given I navigate to "localhost"

  Scenario: Search via Enter key
    Given I select "people" for search
    When I search for "Luke Skywalker" via keypress enter
    Then I should have 1 character results

  Scenario: Existing results change search type and search again
    Given I select "planets" for search
    When I search for "Tatooine"
    Then I should have 1 planet results
    Given I select "people" for search
    When I click search button
    Then I should see no result found

  Scenario: Existing results clear search and search again
    Given I select "planets" for search
    When I search for "Tatooine"
    Then I should have 1 planet results
    When I clear my search and search again
    Then I should see no result found
